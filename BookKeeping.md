# BookKeeping

## 包含页面

1. 展示今日收支情况列表和数据界面
2. 记在每次收入或支出具体情况界面
3. 历史账单记录界面
4. 关于界面
5. 每月账单详情图表界面
6. 搜索界面
7. 设置界面

## 功能介绍

1. 记账
2. 展示账单信息
3. 分析账单情况

## 涉及到的知识点

- 绘制布局

- Activity加载页面信息
  - 生命周期
  - 跳转
  - 传值
- Fragment加载界面
  - ViewPager嵌套Fragment加载数据
  - Fragment切换
- 自定义Dialog
- 自定义软键盘
  - 绘制键盘展示
  - 与指定的EditText绑定使用
- 列表视图适配器的使用
  - ListView/GridView--->BaseAdapter
  - ViewPager-->FragmentPagerAdapter/PagerAdapter
- 数据库的使用
  - 创建表
  - 增删改查
- 定义drawable文件，设定样式
- 使用MPAndroidChart第三方框架绘制柱状图