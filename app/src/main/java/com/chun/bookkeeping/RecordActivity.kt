package com.chun.bookkeeping

import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import android.os.Bundle
import com.chun.bookkeeping.R
import java.util.ArrayList
import com.chun.bookkeeping.recordview.OutcomeFragment
import com.chun.bookkeeping.recordview.IncomeFragment
import com.chun.bookkeeping.adapter.RecordPagerAdapter
import android.view.View
import androidx.fragment.app.Fragment

class RecordActivity : AppCompatActivity() {
    private var mTabLayout: TabLayout? = null
    private var mViewPager: ViewPager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record)
        //查找控件
        mTabLayout = findViewById(R.id.tl_record_tabs)
        mViewPager = findViewById(R.id.vp_record)
        //设置ViewPager加载页面
        initPager()
    }

    private fun initPager() {
//        初始化ViewPager页面的集合
        val fragmentList: MutableList<Fragment> = ArrayList()
        //创建收入和支出页面，放置在Fragment当中
        val outFrag = OutcomeFragment()
        val inFrag = IncomeFragment()
        fragmentList.add(outFrag)
        fragmentList.add(inFrag)

        //创建适配器
        val pagerAdapter = RecordPagerAdapter(supportFragmentManager, fragmentList)
        //设置适配器对象
        mViewPager!!.adapter = pagerAdapter
        //将TabLayout和ViewPager进行关联
        mTabLayout!!.setupWithViewPager(mViewPager)
    }

    //点击事件
    fun onClick(view: View) {
        when (view.id) {
            R.id.iv_record_back -> finish()
        }
    }
}