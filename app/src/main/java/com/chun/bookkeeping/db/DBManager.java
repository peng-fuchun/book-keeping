package com.chun.bookkeeping.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.chun.bookkeeping.utils.FloatUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 负责管理数据库的类
 * 主要对于表当中的内容进行操作，增删改查
 */
public class DBManager {
    private static SQLiteDatabase db;

    /*初始化数据库对象*/
    public static void initDB(Context context){
        DBOpenHelper dbOpenHelper = new DBOpenHelper(context);//得到帮助类对象
        db = dbOpenHelper.getWritableDatabase();//得到数据库对象
    }

    /**
     * 读取数据库当中的数据，写入内存集合里
     * kind：表示收入或支出
     */
    public static List<TypeBean> getTypeList(int kind){
        List<TypeBean>list = new ArrayList<>();
        String sql = "select * from typetb where kind = "+kind;
        Cursor cursor = db.rawQuery(sql, null);
        while (cursor.moveToNext()) {
            int columnTypeName = cursor.getColumnIndex("typename");
            String typename = cursor.getString(columnTypeName);
            int columnImageId = cursor.getColumnIndex("imageId");
            int imageId = cursor.getInt(columnImageId);
            int columnSImageId = cursor.getColumnIndex("sImageId");
            int sImageId = cursor.getInt(columnSImageId);
            int columnKind = cursor.getColumnIndex("kind");
            int kind1 = cursor.getInt(columnKind);
            int columnId = cursor.getColumnIndex("id");
            int id = cursor.getInt(columnId);
            TypeBean typeBean = new TypeBean(id, typename, imageId, sImageId, kind1);
            list.add(typeBean);
        }
        return list;
    }

    /**
     * 向记账表插入一条元素
     */
    public static void insertItemToAccounttb(AccountBean bean){
        ContentValues values = new ContentValues();
        values.put("typename",bean.getTypename());
        values.put("sImageId",bean.getsImageId());
        values.put("beizhu",bean.getBeizhu());
        values.put("money",bean.getMoney());
        values.put("time",bean.getTime());
        values.put("year",bean.getYear());
        values.put("month",bean.getMonth());
        values.put("day",bean.getDay());
        values.put("kind",bean.getKind());
        db.insert("accounttb",null,values);
    }

    /**
     * 获取记账本某一天的所有支出或收入情况
     */
    public static List<AccountBean> getAccountListOneDayFromAccounttb(int year,int month,int day){
        List<AccountBean> list = new ArrayList<>();
        String sql = "select * from accounttb where year=? and month = ? and day = ? order by id desc";
        Cursor cursor = db.rawQuery(sql, new String[]{year + "", month + "", day + ""});
        //遍历符合要求的每一行数据
        while(cursor.moveToNext()){
            int columnId = cursor.getColumnIndex("id");
            int columnTypename = cursor.getColumnIndex("typename");
            int columnBeizhu = cursor.getColumnIndex("beizhu");
            int columnTime = cursor.getColumnIndex("time");
            int columnSImageId = cursor.getColumnIndex("sImageId");
            int columnKind = cursor.getColumnIndex("kind");
            int columnMoney = cursor.getColumnIndex("money");
            int id = cursor.getInt(columnId);
            String typename = cursor.getString(columnTypename);
            String beizhu = cursor.getString(columnBeizhu);
            String time = cursor.getString(columnTime);
            int sImageId = cursor.getInt(columnSImageId);
            int kind = cursor.getInt(columnKind);
            float money = cursor.getFloat(columnMoney);
            AccountBean accountBean = new AccountBean(id, typename, sImageId, beizhu, money, time, year, month, day, kind);
            list.add(accountBean);
        }

        return list;
    }

    /**
     * 获取某一天的支出或收入的总金额
     */
    public static float getSumMoneyOneDay(int year,int month,int day,int kind){
        float total = 0.0f;
        String sql = "select sum(money) from accounttb where year = ? and month = ? and day=? and kind = ?";
        Cursor cursor = db.rawQuery(sql, new String[]{year + "", month + "", day + "", kind + ""});
        if(cursor.moveToFirst()){
            int columnSumMoney = cursor.getColumnIndex("sum(money)");
            float money = cursor.getFloat(columnSumMoney);
            total = money;
        }
        return total;
    }

    /**
     * 获取某一月的支出或收入的总金额
     */
    public static float getSumMoneyOneMonth(int year,int month,int kind){
        float total = 0.0f;
        String sql = "select sum(money) from accounttb where year = ? and month = ? and kind = ?";
        Cursor cursor = db.rawQuery(sql, new String[]{year + "", month + "", kind + ""});
        if(cursor.moveToFirst()){
            int columnSumMoney = cursor.getColumnIndex("sum(money)");
            float money = cursor.getFloat(columnSumMoney);
            total = money;
        }
        return total;
    }

    /**统计某月份支出或收入情况有多少条 收入-1 支出-0*/
    public static int getCountItemOneMonth(int year,int month,int kind){
        int total = 0;
        String sql = "select count(money) from accounttb where year = ? and month = ? and kind = ?";
        Cursor cursor = db.rawQuery(sql, new String[]{year + "", month + "", kind + ""});
        if(cursor.moveToFirst()){
            int columnIndex = cursor.getColumnIndex("count(money)");
            int count = cursor.getInt(columnIndex);
            total = count;
        }
        return total;
    }
    /**
     * 获取某一年的支出或收入的总金额
     */
    public static float getSumMoneyOneYear(int year,int kind){
        float total = 0.0f;
        String sql = "select sum(money) from accounttb where year = ? and kind = ?";
        Cursor cursor = db.rawQuery(sql, new String[]{year + "", kind + ""});
        if(cursor.moveToFirst()){
            int columnSumMoney = cursor.getColumnIndex("sum(money)");
            float money = cursor.getFloat(columnSumMoney);
            total = money;
        }
        return total;
    }
    /**
     * 根据传入的id，删除accounttb表中的一条数据
     */
    public static int deleteItemFromAccounttbById(int id){
        int i = db.delete("accounttb", "id=?", new String[]{id + ""});
        return i;
    }

    /**
     * 根据备注搜索收入或支出的情况列表
     * */
    public static List<AccountBean>getAccountListByRemark(String beizhu){
        List<AccountBean> list = new ArrayList<>();
        String sql = "select * from accounttb where beizhu like '%"+beizhu+"%'";
        Cursor cursor = db.rawQuery(sql, null);
        while (cursor.moveToNext()){
            int columnId = cursor.getColumnIndex("id");
            int columnTypename = cursor.getColumnIndex("typename");
            int columnTime = cursor.getColumnIndex("time");
            int columnSImageId = cursor.getColumnIndex("sImageId");
            int columnKind = cursor.getColumnIndex("kind");
            int columnMoney = cursor.getColumnIndex("money");
            int columnYear = cursor.getColumnIndex("year");
            int columnMonth = cursor.getColumnIndex("month");
            int columnDay = cursor.getColumnIndex("day");
            int columnBeizhu = cursor.getColumnIndex("beizhu");
            int id = cursor.getInt(columnId);
            String typename = cursor.getString(columnTypename);
            String time = cursor.getString(columnTime);
            int sImageId = cursor.getInt(columnSImageId);
            int kind = cursor.getInt(columnKind);
            float money = cursor.getFloat(columnMoney);
            int year = cursor.getInt(columnYear);
            int month = cursor.getInt(columnMonth);
            int day = cursor.getInt(columnDay);
            String bz = cursor.getString(columnBeizhu);
            AccountBean accountBean = new AccountBean(id, typename, sImageId, bz, money, time, year, month, day, kind);
            list.add(accountBean);
        }
        return list;
    }

    /**
     * 获取记账本某一月的所有支出或收入情况
     */
    public static List<AccountBean> getAccountListOneMonthFromAccounttb(int year,int month){
        List<AccountBean> list = new ArrayList<>();
        String sql = "select * from accounttb where year=? and month = ? order by id desc";
        Cursor cursor = db.rawQuery(sql, new String[]{year + "", month + ""});
        //遍历符合要求的每一行数据
        while(cursor.moveToNext()){
            int columnId = cursor.getColumnIndex("id");
            int columnTypename = cursor.getColumnIndex("typename");
            int columnBeizhu = cursor.getColumnIndex("beizhu");
            int columnTime = cursor.getColumnIndex("time");
            int columnSImageId = cursor.getColumnIndex("sImageId");
            int columnKind = cursor.getColumnIndex("kind");
            int columnMoney = cursor.getColumnIndex("money");
            int columnDay = cursor.getColumnIndex("day");
            int id = cursor.getInt(columnId);
            String typename = cursor.getString(columnTypename);
            String beizhu = cursor.getString(columnBeizhu);
            String time = cursor.getString(columnTime);
            int sImageId = cursor.getInt(columnSImageId);
            int kind = cursor.getInt(columnKind);
            float money = cursor.getFloat(columnMoney);
            int day = cursor.getInt(columnDay);
            AccountBean accountBean = new AccountBean(id, typename, sImageId, beizhu, money, time, year, month, day, kind);
            list.add(accountBean);
        }
        return list;
    }

    /**
     * 查询记账表当中有几个年份信息
     * */
    public static List<Integer> getYearListFromAccounttb(){
        List<Integer>list = new ArrayList<>();
        String sql = "select distinct(year) from accounttb order by year asc";
        Cursor cursor = db.rawQuery(sql, null);
        while(cursor.moveToNext()){
            int columnYear = cursor.getColumnIndex("year");
            int year = cursor.getInt(columnYear);
            list.add(year);
        }
        return list;
    }

    /**
     * 删除所有数据
     */
    public static void deleteAllData(){
        String sql = "delete from accounttb";
        db.execSQL(sql);
    }

    /**
     * 查询指定年份和月份的收入或支出每一种类型的总钱数
     * */
    public static List<ChartItemBean>getChartListFromAccounttb(int year,int month,int kind){
        List<ChartItemBean> list = new ArrayList<>();
        float sumMoneyOneMonth = getSumMoneyOneMonth(year, month, kind);
        String sql = "select typename,sImageId,sum(money) as total from accounttb where year = ? and month = ? and kind = ? group by typename " +
                "order by total desc";
        Cursor cursor = db.rawQuery(sql, new String[]{year + "", month + "", kind + ""});
        while (cursor.moveToNext()){
            int columnSImageId = cursor.getColumnIndex("sImageId");
            int columnTypename = cursor.getColumnIndex("typename");
            int columnTotal = cursor.getColumnIndex("total");
            int simageId = cursor.getInt(columnSImageId);
            String typename = cursor.getString(columnTypename);
            float total = cursor.getFloat(columnTotal);
            //计算所占百分比
            float ratio = FloatUtils.div(total,sumMoneyOneMonth);
            ChartItemBean bean = new ChartItemBean(simageId, typename, ratio, total);
            list.add(bean);
        }
        return list;
    }

    /**获取本月当中收入支出最大的金额*/
    public static float getMaxMoneyOneDayInMonth(int year,int month,int kind){
        String sql = "select sum(money) from accounttb where year = ? and month = ? and kind = ? group by day order by sum(money) desc";
        Cursor cursor = db.rawQuery(sql, new String[]{year + "", month + "", kind + ""});
        if(cursor.moveToFirst()){
            int columnIndex = cursor.getColumnIndex("sum(money)");
            float money = cursor.getFloat(columnIndex);
            return money;
        }
        return 0;
    }

    /**根据指定月份获取每日收入或支出的总钱数的金额*/
    public static List<BarChartItemBean>getSumMoneyOneDayInMonth(int year,int month,int kind){
        String sql = "select day,sum(money) from accounttb where year = ? and month = ? and kind = ? group by day";
        Cursor cursor = db.rawQuery(sql, new String[]{year + "", month + "", kind + ""});
        List<BarChartItemBean> list = new ArrayList<>();
        while (cursor.moveToNext()){
            int columnDay = cursor.getColumnIndex("day");
            int columnSum = cursor.getColumnIndex("sum(money)");
            int day = cursor.getInt(columnDay);
            float sumMoney = cursor.getFloat(columnSum);
            BarChartItemBean bean = new BarChartItemBean(year, month, day, sumMoney);
            list.add(bean);
        }
        return list;
    }
}
