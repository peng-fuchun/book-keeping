package com.chun.bookkeeping;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chun.bookkeeping.adapter.AccountAdapter;
import com.chun.bookkeeping.db.AccountBean;
import com.chun.bookkeeping.db.DBManager;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

    private ListView searchLv;
    private EditText searchEt;
    private TextView emptyTv;
    private List<AccountBean>mDatas; //数据源
    private AccountAdapter adapter;  //适配器
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initView();
        mDatas = new ArrayList<>();
        adapter = new AccountAdapter(this,mDatas);
        searchLv.setAdapter(adapter);
        searchLv.setEmptyView(emptyTv);
    }

    private void initView() {
        searchLv = findViewById(R.id.lv_search);
        searchEt = findViewById(R.id.et_search);
        emptyTv = findViewById(R.id.tv_search_empty);
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_search_back:
                finish();
                break;
            case R.id.iv_search_sh:
                String msg = searchEt.getText().toString().trim();
                if(TextUtils.isEmpty(msg)){
                    Toast.makeText(this, R.string.input_content_empty, Toast.LENGTH_SHORT).show();
                    return;
                }
                //开始搜索
                List<AccountBean> list = DBManager.getAccountListByRemark(msg);
                mDatas.clear();
                mDatas.addAll(list);
                adapter.notifyDataSetChanged();
                break;
        }
    }
}