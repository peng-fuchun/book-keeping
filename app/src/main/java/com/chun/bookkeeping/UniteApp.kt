package com.chun.bookkeeping

import android.app.Application
import com.chun.bookkeeping.db.DBManager

class UniteApp : Application() {
    override fun onCreate() {
        super.onCreate()
        DBManager.initDB(applicationContext)
    }
}