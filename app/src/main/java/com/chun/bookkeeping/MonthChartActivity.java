package com.chun.bookkeeping;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.chun.bookkeeping.adapter.ChartVPAdapter;
import com.chun.bookkeeping.chartfrag.IncomeChartFragment;
import com.chun.bookkeeping.chartfrag.OutcomeChartFragment;
import com.chun.bookkeeping.db.DBManager;
import com.chun.bookkeeping.recordview.IncomeFragment;
import com.chun.bookkeeping.utils.CalendarDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MonthChartActivity extends AppCompatActivity {
    private Button inBtn,outBtn;
    private TextView dateTv,inTv,outTv;
    private ViewPager chartVp;
    int selectPos=-1,selectMonth=-1;
    private int year;
    private int month;
    List<Fragment>chartFragList;
    private IncomeChartFragment incomeChartFragment;
    private OutcomeChartFragment outcomeChartFragment;
    private ChartVPAdapter chartVPAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_month_chart);
        initView();
        initTime();
        initStatistics(year,month);
        initFrag();
        setVPSelectListener();
    }

    private void setVPSelectListener() {
        chartVp.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                setButtonStyle(position);
            }
        });
    }

    private void initFrag() {
        chartFragList = new ArrayList<>();
        incomeChartFragment = new IncomeChartFragment();
        outcomeChartFragment = new OutcomeChartFragment();

        Bundle bundle = new Bundle();
        bundle.putInt("year",year);
        bundle.putInt("month",month);
        incomeChartFragment.setArguments(bundle);
        outcomeChartFragment.setArguments(bundle);
        chartFragList.add(outcomeChartFragment);
        chartFragList.add(incomeChartFragment);
        //添加适配器
        chartVPAdapter = new ChartVPAdapter(getSupportFragmentManager(),chartFragList);
        chartVp.setAdapter(chartVPAdapter);
        //讲Fragment添加到Activity
    }

    /**
     * 初始化某年某月的收支情况数据*/
    private void initStatistics(int year, int month) {
        float inMoneyOneMonth = DBManager.getSumMoneyOneMonth(year, month, 1);//收入总钱数
        float outMoneyOneMonth = DBManager.getSumMoneyOneMonth(year, month, 0);//支出总钱数
        int inCountItemOneMonth = DBManager.getCountItemOneMonth(year, month, 1);//收入多少笔
        int outCountItemOneMonth = DBManager.getCountItemOneMonth(year, month, 0);//支出多少笔
        dateTv.setText(year+"年"+month+"月账单");
        inTv.setText("共"+inCountItemOneMonth+"笔收入，￥"+inMoneyOneMonth);
        outTv.setText("共"+outCountItemOneMonth+"笔收入,￥"+outMoneyOneMonth);
    }
    /**初始化时间的方法*/
    private void initTime() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH)+1;
    }

    private void initView() {
        inBtn = findViewById(R.id.btn_chart_in);
        outBtn = findViewById(R.id.btn_chart_out);
        dateTv = findViewById(R.id.tv_chart_date);
        inTv = findViewById(R.id.tv_chart_in);
        outTv = findViewById(R.id.tv_chart_out);
        chartVp = findViewById(R.id.vp_chart);
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_chart_in:
                setButtonStyle(1);
                chartVp.setCurrentItem(1);
                break;
            case R.id.btn_chart_out:
                setButtonStyle(0);
                chartVp.setCurrentItem(0);
                break;
            case R.id.iv_chart_calendar:
                showCalendarDialog();
                break;
            case R.id.iv_chart_back:
                finish();
                break;
        }
    }

    private void showCalendarDialog() {
        CalendarDialog dialog = new CalendarDialog(this, selectPos, selectMonth);
        dialog.show();
        dialog.setDialogSize();
        dialog.setOnRefreshListener(new CalendarDialog.onRefreshListener() {
            @Override
            public void onRefresh(int selPos, int year, int month) {
                MonthChartActivity.this.selectMonth = month;
                MonthChartActivity.this.selectPos = selPos;
                initStatistics(year,month);
                incomeChartFragment.setDate(year,month);
                outcomeChartFragment.setDate(year,month);
            }
        });
    }

    /*设置按钮样式的改变 支出-0 收入-1*/
    private void setButtonStyle(int kind){
        if(kind==0){
            outBtn.setBackgroundResource(R.drawable.bg_main_recordbtn);
            outBtn.setTextColor(Color.WHITE);
            inBtn.setBackgroundResource(R.drawable.bg_dialog_btn_ensure);
            inBtn.setTextColor(Color.BLACK);
        }else{
            inBtn.setBackgroundResource(R.drawable.bg_main_recordbtn);
            inBtn.setTextColor(Color.WHITE);
            outBtn.setBackgroundResource(R.drawable.bg_dialog_btn_ensure);
            outBtn.setTextColor(Color.BLACK);
        }
    }
}