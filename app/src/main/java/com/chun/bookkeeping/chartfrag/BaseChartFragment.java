package com.chun.bookkeeping.chartfrag;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.chun.bookkeeping.R;
import com.chun.bookkeeping.adapter.ChartItemAdapter;
import com.chun.bookkeeping.db.ChartItemBean;
import com.chun.bookkeeping.db.DBManager;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;
import java.util.List;

abstract public class BaseChartFragment extends Fragment {
    private ListView chartLv;
    int year;
    int month;
    private List<ChartItemBean> mDatas;
    private ChartItemAdapter itemAdapter;
    BarChart barChart; //代表柱状图的控件
    TextView chartTv;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_income, container, false);
        chartLv = view.findViewById(R.id.lv_frag_chart_in);
        Bundle bundle = getArguments();
        year = bundle.getInt("year");
        month = bundle.getInt("month");
        //设置数据源
        mDatas = new ArrayList<>();
        //设置适配器
        itemAdapter = new ChartItemAdapter(getContext(),mDatas);
        chartLv.setAdapter(itemAdapter);
        //添加头布局
        addLVHeaderView();
        return view;
    }

    private void addLVHeaderView() {
        //讲布局转换为View对象
        View headerView = getLayoutInflater().inflate(R.layout.item_chartfrag_top, null);
        //将View添加到ListView的头布局上
        chartLv.addHeaderView(headerView);
        //查找头布局当中的控件
        barChart = headerView.findViewById(R.id.bc_item_chartfrag_top);
        chartTv = headerView.findViewById(R.id.tv_item_chartfrag_top);
        //设定柱状图不显示描述
        barChart.getDescription().setEnabled(false);
        //设置柱状图的内边距
        barChart.setExtraOffsets(20,20,20,20);
        setAxis(year,month);//设置坐标轴
        //设置坐标轴显示的数据
        setAxisData(year,month);
    }
    /**设置坐标轴显示的数据*/
    protected abstract void setAxisData(int year, int month);

    /**设置柱状图坐标轴的显示,方法必须重写*/
    private void setAxis(int year,int month) {
        //设置X轴
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);//设置x轴显示在下方
        xAxis.setDrawGridLines(true);//设置绘制该轴的网格线
        //设置x轴标签的个数
        xAxis.setLabelCount(31);
        xAxis.setTextSize(12f);//x轴标签的大小
        //设置X轴显示的值的格式
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float v, AxisBase axisBase) {
                int val = (int) v;
                if(val==0){
                    return month+"-1";
                }
                if(val==14){
                    return month+"15";
                }
                if(month==2){
                    if(val==27){
                        return month+"-28";
                    }
                }else if(month==1||month==3||month==5||month==7||month==8||month==10||month==12){
                    if (val==30){
                        return month+"-31";
                    }
                }else if(month==4||month==6||month==9||month==11){
                    if(val==29){
                        return month+"-30";
                    }
                }
                return "";
            }
        });
        xAxis.setYOffset(10);//设置标签对x轴的偏移量
        //y轴在子类的设置
        setYAxis(year,month);
    }

    protected abstract void setYAxis(int year,int month);

    public void setDate(int year,int month){
        this.year = year;
        this.month = month;
        barChart.clear();
        barChart.invalidate();
        setAxis(year,month);
        setAxisData(year,month);
    }

    public void loadData(int year,int month,int kind) {
        List<ChartItemBean> list = DBManager.getChartListFromAccounttb(year, month, kind);
        mDatas.clear();
        mDatas.addAll(list);
        itemAdapter.notifyDataSetChanged();
    }
}
