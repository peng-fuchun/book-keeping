package com.chun.bookkeeping.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chun.bookkeeping.R;
import com.chun.bookkeeping.db.AccountBean;

import java.util.Calendar;
import java.util.List;

public class AccountAdapter extends BaseAdapter {
    private Context context;
    private List<AccountBean> mDatas;
    private LayoutInflater inflater;
    int year,month,day;
    public AccountAdapter(Context context, List<AccountBean> mDatas) {
        this.context = context;
        this.mDatas = mDatas;
        inflater = LayoutInflater.from(context);
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH)+1;
        day = calendar.get(Calendar.DAY_OF_MONTH);
    }

    @Override
    public int getCount() {
        return mDatas==null?0:mDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AccountViewHolder accountViewHolder = null;
        if(convertView==null){
            convertView = inflater.inflate(R.layout.lv_item_main,null,false);
            accountViewHolder = new AccountViewHolder(convertView);
            convertView.setTag(accountViewHolder);
        }else{
            accountViewHolder = (AccountViewHolder) convertView.getTag();
        }
        AccountBean accountBean = mDatas.get(position);
        accountViewHolder.typeIv.setImageResource(accountBean.getsImageId());
        accountViewHolder.typeTv.setText(accountBean.getTypename());
        accountViewHolder.beizhuTv.setText(accountBean.getBeizhu());
        accountViewHolder.moneyTv.setText("￥ "+accountBean.getMoney());
        if (accountBean.getYear()==year&&accountBean.getMonth()==month&&accountBean.getDay()==day) {
            String time = accountBean.getTime().split("日")[1];
            accountViewHolder.timeTv.setText("今天 "+time);
        }else{
            accountViewHolder.timeTv.setText(accountBean.getTime());
        }
        return convertView;
    }

    class AccountViewHolder{
        ImageView typeIv;
        TextView typeTv,beizhuTv,timeTv,moneyTv;
        public AccountViewHolder(View view){
            typeIv = view.findViewById(R.id.iv_lv_item_main);
            typeTv = view.findViewById(R.id.tv_item_main_title);
            beizhuTv = view.findViewById(R.id.tv_item_main_remarks);
            timeTv = view.findViewById(R.id.tv_item_main_time);
            moneyTv = view.findViewById(R.id.tv_item_main_money);
        }
    }
}
