package com.chun.bookkeeping.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chun.bookkeeping.R;
import com.chun.bookkeeping.db.TypeBean;

import java.util.List;

public class TypeBaseAdapter extends BaseAdapter {

    public int selectPosition = 0;//选中位置
    private Context context;
    private List<TypeBean> mDatas;
    public TypeBaseAdapter(Context context, List<TypeBean> mDatas) {
        this.context = context;
        this.mDatas = mDatas;
    }

    @Override
    public int getCount() {
        return mDatas==null?0:mDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    //此适配器不考虑复用问题，因为所有的item都显示在界面上，不会因为滑动就消失，所有没有剩余的convertView，所以不用复写
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.gv_frag_item_record,parent,false);
        //查找布局中控件
        ImageView imageView = convertView.findViewById(R.id.iv_item_record_frag);
        TextView textView = convertView.findViewById(R.id.tv_item_record_frag);
        //获取指定位置数据源
        TypeBean typeBean = mDatas.get(position);
        textView.setText(typeBean.getTypename());
        //判断当前位置是否为选中位置，若为选中位置，就设置为带颜色的图片，否则为灰色
        if(selectPosition==position){
            imageView.setImageResource(typeBean.getSimageId());
        }else{
            imageView.setImageResource(typeBean.getImageId());
        }
        return convertView;
    }
}
