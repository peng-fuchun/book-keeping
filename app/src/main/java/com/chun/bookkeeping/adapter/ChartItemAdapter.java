package com.chun.bookkeeping.adapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chun.bookkeeping.R;
import com.chun.bookkeeping.db.ChartItemBean;
import com.chun.bookkeeping.utils.FloatUtils;

import org.w3c.dom.Text;

import java.util.List;

public class ChartItemAdapter extends BaseAdapter {
    private Context context;
    private List<ChartItemBean> mDatas;
    private LayoutInflater inflater;
    public ChartItemAdapter(Context context, List<ChartItemBean> mDatas) {
        this.context = context;
        this.mDatas = mDatas;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ChartViewHolder holder = null;
        if(convertView==null){
            convertView = inflater.inflate(R.layout.item_chartfrag,parent,false);
            holder = new ChartViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ChartViewHolder) convertView.getTag();
        }
        //获取显示内容
        ChartItemBean bean = mDatas.get(position);
        holder.iv.setImageResource(bean.getsImageId());
        float ratio = bean.getRatio();
        String pert = FloatUtils.ratioToPercent(ratio);
        holder.ratioTv.setText(pert);
        holder.totalTv.setText("￥ "+bean.getTotalMoney());
        holder.typeTv.setText(bean.getType());
        return convertView;
    }

    class ChartViewHolder{
        TextView typeTv,ratioTv,totalTv;
        ImageView iv;
        public ChartViewHolder(View view){
            typeTv = view.findViewById(R.id.tv_item_chartfrag);
            ratioTv = view.findViewById(R.id.tv_item_chartfrag_pert);
            totalTv = view.findViewById(R.id.tv_item_chartfrag_sum);
            iv = view.findViewById(R.id.iv_item_chartfrag);
        }
    }
}
