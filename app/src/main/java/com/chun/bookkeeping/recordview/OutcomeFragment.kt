package com.chun.bookkeeping.recordview

import com.chun.bookkeeping.db.DBManager
import com.chun.bookkeeping.R

/**
 * 支出模块
 */
class OutcomeFragment : BaseRecordFragment() {
    override fun loadDataToGV() {
        super.loadDataToGV()
        //获取数据库的数据源
        val outList = DBManager.getTypeList(0)
        typeList!!.addAll(outList)
        adapter!!.notifyDataSetChanged()
        typeTv!!.text = "其他"
        typeIv!!.setImageResource(R.mipmap.ic_qita_fs)
    }

    override fun saveAccountToDB() {
        accountBean!!.kind = 0
        DBManager.insertItemToAccounttb(accountBean)
    }
}