package com.chun.bookkeeping.recordview

import android.view.View
import android.inputmethodservice.KeyboardView
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.GridView
import com.chun.bookkeeping.db.TypeBean
import com.chun.bookkeeping.adapter.TypeBaseAdapter
import com.chun.bookkeeping.db.AccountBean
import android.os.Bundle
import com.chun.bookkeeping.R
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView.OnItemClickListener
import android.widget.AdapterView
import com.chun.bookkeeping.utils.KeyboardUtils
import com.chun.bookkeeping.utils.KeyboardUtils.OnEnsureListener
import android.text.TextUtils
import androidx.fragment.app.Fragment
import com.chun.bookkeeping.utils.SelectTimeDialog
import com.chun.bookkeeping.utils.SelectTimeDialog.OnTimeEnsureListener
import com.chun.bookkeeping.utils.BeiZhuDialog
import com.chun.bookkeeping.utils.BeiZhuDialog.OnRemarksEnsureListener
import java.text.SimpleDateFormat
import java.util.*

/**
 * 记录页面当中的支出模块
 */
abstract class BaseRecordFragment : Fragment(), View.OnClickListener {
    private var mKeyboardView: KeyboardView? = null
    private var moneyEt: EditText? = null
    var typeIv: ImageView? = null
    var typeTv: TextView? = null
    var remarksTv: TextView? = null
    var timeTv: TextView? = null
    private var typeGv: GridView? = null
    var typeList: MutableList<TypeBean>? = null
    var adapter: TypeBaseAdapter? = null
    var accountBean //记账本数据
            : AccountBean? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        accountBean = AccountBean()
        accountBean!!.typename = "其他"
        accountBean!!.setsImageId(R.mipmap.ic_qita_fs)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_outcome, container, false)
        initView(view)
        setInitTime()
        loadDataToGV()
        setGVListener()
        return view
    }

    /**
     * 获取当前时间 显示在timeTv上
     */
    private fun setInitTime() {
        val date = Date()
        val sdf = SimpleDateFormat("yyyy年MM月dd日 HH:mm")
        val time = sdf.format(date)
        timeTv!!.text = time
        accountBean!!.time = time
        val calendar = Calendar.getInstance()
        val year = calendar[Calendar.YEAR]
        val month = calendar[Calendar.MONTH] + 1
        val day = calendar[Calendar.DAY_OF_MONTH]
        accountBean!!.year = year
        accountBean!!.month = month
        accountBean!!.day = day
    }

    /**
     * 设置GridView每一项的点击事件
     */
    private fun setGVListener() {
        typeGv!!.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            adapter!!.selectPosition = position
            adapter!!.notifyDataSetInvalidated() //提示绘制发生变化
            val typeBean = typeList!![position]
            typeTv!!.text = typeBean.typename
            accountBean!!.typename = typeBean.typename
            typeIv!!.setImageResource(typeBean.simageId)
            accountBean!!.setsImageId(typeBean.simageId)
        }
    }

    /**
     * 给GridView添加数据
     */
    open fun loadDataToGV() {
        typeList = ArrayList()
        adapter = TypeBaseAdapter(context, typeList)
        typeGv!!.adapter = adapter
    }

    private fun initView(view: View) {
        mKeyboardView = view.findViewById(R.id.kv_frag_record)
        moneyEt = view.findViewById(R.id.et_frag_record)
        typeIv = view.findViewById(R.id.iv_frag_record)
        typeGv = view.findViewById(R.id.gv_frag_record)
        typeTv = view.findViewById(R.id.tv_frag_record_type)
        remarksTv = view.findViewById(R.id.tv_frag_record_remarks)
        timeTv = view.findViewById(R.id.tv_frag_record_time)
        remarksTv?.setOnClickListener(this)
        timeTv?.setOnClickListener(this)
        val keyboardUtils = KeyboardUtils(mKeyboardView, moneyEt)
        keyboardUtils.showKeyboard()
        //设置接口，监听确定监听按钮
        keyboardUtils.setOnEnsureListener(OnEnsureListener {
            val moneyStr = moneyEt?.getText().toString()
            if (TextUtils.isEmpty(moneyStr) || moneyStr == "0") {
                activity!!.finish()
                return@OnEnsureListener
            }
            val money = moneyStr.toFloat()
            accountBean!!.money = money
            saveAccountToDB()
            activity!!.finish()
        })
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tv_frag_record_time -> showTimeDialog()
            R.id.tv_frag_record_remarks -> showBZDialog()
            else -> {
            }
        }
    }

    /**
     * 展示时间选择对话框
     */
    private fun showTimeDialog() {
        val timeDialog = SelectTimeDialog(context!!)
        timeDialog.show()
        timeDialog.setOnTimeEnsureListener { time, year, month, day ->
            timeTv!!.text = time
            accountBean!!.time = time
            accountBean!!.year = year
            accountBean!!.month = month
            accountBean!!.day = day
        }
    }

    /**
     * 弹出备注对话框
     */
    private fun showBZDialog() {
        val dialog = BeiZhuDialog(context!!)
        dialog.show()
        dialog.setDialogSize()
        dialog.setOnEnsureListener {
            val remarks = dialog.remarks
            if (!TextUtils.isEmpty(remarks)) {
                remarksTv!!.text = remarks
                accountBean!!.beizhu = remarks
            }
            dialog.cancel()
        }
    }

    abstract fun saveAccountToDB()
}