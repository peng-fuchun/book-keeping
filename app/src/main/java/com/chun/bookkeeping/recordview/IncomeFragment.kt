package com.chun.bookkeeping.recordview

import com.chun.bookkeeping.recordview.BaseRecordFragment
import com.chun.bookkeeping.db.TypeBean
import com.chun.bookkeeping.db.DBManager
import com.chun.bookkeeping.R

/**
 * 收入模块
 */
class IncomeFragment : BaseRecordFragment() {
    override fun loadDataToGV() {
        super.loadDataToGV()
        //获取数据库的数据源
        val outList = DBManager.getTypeList(1)
        typeList!!.addAll(outList)
        adapter!!.notifyDataSetChanged()
        typeTv!!.text = "其他"
        typeIv!!.setImageResource(R.mipmap.in_qt_fs)
    }

    override fun saveAccountToDB() {
        accountBean!!.kind = 1
        DBManager.insertItemToAccounttb(accountBean)
    }
}