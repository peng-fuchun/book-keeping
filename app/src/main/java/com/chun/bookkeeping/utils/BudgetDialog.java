package com.chun.bookkeeping.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.chun.bookkeeping.R;

public class BudgetDialog extends Dialog implements View.OnClickListener {

    private ImageView ivBudgetCancel;
    private Button btnBudgetEnsure;
    private EditText etBudget;
    private OnBudgetEnsureListener onBudgetEnsureListener;

    public void setOnBudgetEnsureListener(OnBudgetEnsureListener onBudgetEnsureListener) {
        this.onBudgetEnsureListener = onBudgetEnsureListener;
    }

    public BudgetDialog(@NonNull Context context) {
        super(context);
        setContentView(R.layout.dialog_budget);
        ivBudgetCancel = findViewById(R.id.iv_dialog_budget_cancel);
        btnBudgetEnsure = findViewById(R.id.btn_dialog_budget_ensure);
        etBudget = findViewById(R.id.et_dialog_set_budget);
        ivBudgetCancel.setOnClickListener(this);
        btnBudgetEnsure.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_dialog_budget_cancel:
                cancel();
                break;
            case R.id.btn_dialog_budget_ensure:
                //获取输入数据数值
                String data = etBudget.getText().toString();
                if (TextUtils.isEmpty(data)) {
                    Toast.makeText(getContext(), R.string.data_can_not_empty, Toast.LENGTH_SHORT).show();
                    return;
                }
                float budget = Float.parseFloat(data);
                if(budget<=0){
                    Toast.makeText(getContext(),R.string.data_must_than_zero,Toast.LENGTH_SHORT).show();
                    return;
                }
                if (onBudgetEnsureListener!=null) {
                    onBudgetEnsureListener.onEnsure(budget);
                }
                cancel();
                break;
            default:
                break;
        }

    }
    public interface OnBudgetEnsureListener{
        public void onEnsure(float money);
    }

    /**
     * 设置Dialog尺寸和屏幕尺寸一致
     */
    public void setDialogSize(){
        //获取当前窗口对象
        Window window = getWindow();
//        获取窗口对象参数
        WindowManager.LayoutParams wlp = window.getAttributes();
//        获取屏幕宽度
        Display display = window.getWindowManager().getDefaultDisplay();
        wlp.width = (int)(display.getWidth());//对话框窗口为屏幕窗口
        wlp.gravity = Gravity.BOTTOM;
        window.setBackgroundDrawableResource(android.R.color.transparent);
        window.setAttributes(wlp);
        handler.sendEmptyMessageDelayed(1,100);
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            //自动弹出软键盘
            InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInput(0,InputMethodManager.HIDE_NOT_ALWAYS);
        }
    };
}
