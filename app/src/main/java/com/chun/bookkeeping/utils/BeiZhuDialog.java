package com.chun.bookkeeping.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.chun.bookkeeping.R;

public class BeiZhuDialog extends Dialog implements View.OnClickListener {
    EditText et;
    Button cancelBtn,ensureBtn;
    private OnRemarksEnsureListener onEnsureListener;

    public void setOnEnsureListener(OnRemarksEnsureListener onEnsureListener) {
        this.onEnsureListener = onEnsureListener;
    }

    public BeiZhuDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_beizhu);
        et = findViewById(R.id.et_dialog_beizhu);
        cancelBtn = findViewById(R.id.btn_dialog_cancel);
        ensureBtn = findViewById(R.id.btn_dialog_ensure);
        cancelBtn.setOnClickListener(this);
        ensureBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_dialog_cancel:
                cancel();
                break;
            case R.id.btn_dialog_ensure:
                if (onEnsureListener!=null){
                    onEnsureListener.onEnsure();
                }
                break;
            default:
                break;
        }
    }

    /**
     * 获取输入的备注数据
     */
    public String getRemarks(){
        return et.getText().toString().trim();
    }

    public interface OnRemarksEnsureListener {
        public void onEnsure();
    }

    /**
     * 设置Dialog尺寸和屏幕尺寸一致
     */
    public void setDialogSize(){
        //获取当前窗口对象
        Window window = getWindow();
//        获取窗口对象参数
        WindowManager.LayoutParams wlp = window.getAttributes();
//        获取屏幕宽度
        Display display = window.getWindowManager().getDefaultDisplay();
        wlp.width = (int)(display.getWidth());//对话框窗口为屏幕窗口
        wlp.gravity = Gravity.BOTTOM;
        window.setBackgroundDrawableResource(android.R.color.transparent);
        window.setAttributes(wlp);
        handler.sendEmptyMessageDelayed(1,100);
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            //自动弹出软键盘
            InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInput(0,InputMethodManager.HIDE_NOT_ALWAYS);
        }
    };
}
