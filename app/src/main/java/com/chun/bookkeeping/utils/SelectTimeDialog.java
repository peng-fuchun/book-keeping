package com.chun.bookkeeping.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.chun.bookkeeping.R;

/**
 * 在记录页面弹出事件对话框
 */
public class SelectTimeDialog extends Dialog implements View.OnClickListener {

    private EditText hourEt,minuteEt;
    private DatePicker datePicker;
    private Button ensureBtn,cancelBtn;
    private OnTimeEnsureListener onTimeEnsureListener;

    public SelectTimeDialog(@NonNull Context context) {
        super(context);
    }

    public void setOnTimeEnsureListener(OnTimeEnsureListener onTimeEnsureListener) {
        this.onTimeEnsureListener = onTimeEnsureListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_time);
        hourEt = findViewById(R.id.et_dialog_time_hour);
        minuteEt = findViewById(R.id.et_dialog_time_minute);
        datePicker = findViewById(R.id.dp_dialog_time);
        ensureBtn = findViewById(R.id.btn_dialog_time_ensure);
        cancelBtn = findViewById(R.id.btn_dialog_time_cancel);
        ensureBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
        hideDatePickerHeader();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_dialog_time_ensure:
                int year = datePicker.getYear();
                int month = datePicker.getMonth()+1;
                int day = datePicker.getDayOfMonth();
                String monthStr = String.valueOf(month);
                if(month<10){
                    monthStr = "0"+month;
                }
                String dayStr = String.valueOf(day);
                if(day<10){
                    dayStr = "0"+day;
                }
                String hourStr = hourEt.getText().toString();
                String minuteStr = minuteEt.getText().toString();
                int hour = 0;
                if (!TextUtils.isEmpty(hourStr)) {
                    hour = Integer.parseInt(hourStr);
                    hour = hour%24;
                }
                int minutes = 0;
                if(!TextUtils.isEmpty(minuteStr)){
                    minutes = Integer.parseInt(minuteStr);
                    minutes = minutes%60;
                }
                hourStr = String.valueOf(hour);
                minuteStr = String.valueOf(minutes);
                if(hour<10){
                    hourStr = "0"+hour;
                }
                if(minutes<10){
                    minuteStr = "0"+minutes;
                }
                String timeFormat = year+"年"+monthStr+"月"+dayStr+"日"+hourStr+":"+minuteStr;
                if (onTimeEnsureListener!=null) {
                    onTimeEnsureListener.onEnsure(timeFormat,year,month,day);
                }
                cancel();
                break;
            case R.id.btn_dialog_time_cancel:
                cancel();
                break;
            default:
                break;
        }
    }

    //隐藏DatePicker头布局
    private void hideDatePickerHeader(){
        ViewGroup rootView = (ViewGroup)datePicker.getChildAt(0);
        if (rootView==null) {
            return;
        }
        View headerView = rootView.getChildAt(0);
        if (headerView==null) {
            return;
        }
        //5.0+
        int headerId = getContext().getResources().getIdentifier("day_picker_selector_layout", "id", "android");
        if (headerId == headerView.getId()) {
            headerView.setVisibility(View.GONE);
            ViewGroup.LayoutParams layoutParams = rootView.getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            rootView.setLayoutParams(layoutParams);

            ViewGroup animator = (ViewGroup)rootView.getChildAt(1);
            ViewGroup.LayoutParams animatorLayoutParams = animator.getLayoutParams();
            animatorLayoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            animator.setLayoutParams(animatorLayoutParams);

            View child = animator.getChildAt(0);
            ViewGroup.LayoutParams childLayoutParams = child.getLayoutParams();
            childLayoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            child.setLayoutParams(childLayoutParams);
            return;
        }
        //6.0+
        headerId = getContext().getResources().getIdentifier("date_picker_header","id","android");
        if(headerId==headerView.getId()){
            headerView.setVisibility(View.GONE);
        }
    }
    public interface OnTimeEnsureListener{
        public void onEnsure(String time,int year,int month,int day);
    }


}
