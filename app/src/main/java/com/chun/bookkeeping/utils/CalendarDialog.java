package com.chun.bookkeeping.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.chun.bookkeeping.R;
import com.chun.bookkeeping.adapter.CalendarAdapter;
import com.chun.bookkeeping.db.DBManager;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CalendarDialog extends Dialog implements View.OnClickListener {
    private ImageView cancelIv;
    private GridView calendarGv;
    private LinearLayout hsvLl;
    private CalendarAdapter calendarAdapter;
    int selectMonth = -1;

    List<TextView> hsvViewList;
    List<Integer> yearList;
    int selectPos = -1;//表示正在被点击得年份的位置
    public CalendarDialog(@NonNull Context context,int selectPos,int selectMonth) {
        super(context);
        this.selectPos = selectPos;
        this.selectMonth = selectMonth;
    }
    private onRefreshListener onRefreshListener;

    public void setOnRefreshListener(CalendarDialog.onRefreshListener onRefreshListener) {
        this.onRefreshListener = onRefreshListener;
    }

    public interface onRefreshListener{
        //刷新日历选中的位置年月
        public void onRefresh(int selPos,int year,int month);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_calendar);
        calendarGv = findViewById(R.id.gv_dialog_calendar);
        cancelIv = findViewById(R.id.iv_dialog_calendar_cancel);
        hsvLl = findViewById(R.id.ll_dialog_calendar);
        cancelIv.setOnClickListener(this);
        addViewToLayout();
        initGridView();
        setGVListener();
    }

    private void setGVListener() {
        calendarGv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                calendarAdapter.selPos = position;
                calendarAdapter.notifyDataSetInvalidated();
                //获取到被选中的年份和月份
                int month = position + 1;
                int year = calendarAdapter.year;
                onRefreshListener.onRefresh(selectPos,year,month);
                cancel();
            }
        });
    }

    private void initGridView() {
        int selYear = yearList.get(selectPos);
        calendarAdapter = new CalendarAdapter(getContext(),selYear);
        if(selectMonth==-1){
            int month = Calendar.getInstance().get(Calendar.MONTH);
            calendarAdapter.selPos = month;
        }else{
            calendarAdapter.selPos = selectMonth - 1;
        }
        calendarGv.setAdapter(calendarAdapter);
    }

    private void addViewToLayout() {
        hsvViewList = new ArrayList<>();
        yearList = DBManager.getYearListFromAccounttb();
        if(yearList.size()==0){
            int year = Calendar.getInstance().get(Calendar.YEAR);
            yearList.add(year);
        }
        //便利年份，有几年，就在ScrollView中添加几个View
        for (int i = 0; i < yearList.size(); i++) {
            int year = yearList.get(i);
            View view = getLayoutInflater().inflate(R.layout.item_dialogcal_hsv, null);
            hsvLl.addView(view);
            TextView hsvTv = view.findViewById(R.id.tv_item_dialogcal_hsv);
            hsvTv.setText(year+"");
            hsvViewList.add(hsvTv);
        }
        if (selectPos==-1) {
            selectPos=hsvViewList.size()-1;//设置位最近的年份
        }
        changeTvbg(selectPos); //将最后一个设置为选中状态
        setHSVClickListener(); //设置每一个View的监听事件
    }

    /**
     * 给横向的ScrollView当中的每一个TextView设置点击事件
     */
    private void setHSVClickListener() {
        for (int i = 0; i < hsvViewList.size(); i++) {
            TextView view = hsvViewList.get(i);
            final int pos = i;
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeTvbg(pos);
                    selectPos = pos;
                    int year = yearList.get(selectPos);
                    calendarAdapter.setYear(year);
                }
            });
        }
    }

    /**
     * 传入被选中的位置，改变此为止上的背景和文字颜色
     * @param selectPos
     */
    private void changeTvbg(int selectPos) {
        for (int i = 0; i < hsvViewList.size(); i++) {
            TextView tv = hsvViewList.get(i);
            tv.setBackgroundResource(R.drawable.bg_dialog_btn_ensure);
            tv.setTextColor(Color.BLACK);
        }
        TextView selView = hsvViewList.get(selectPos);
        selView.setBackgroundResource(R.drawable.bg_main_recordbtn);
        selView.setTextColor(Color.WHITE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_dialog_calendar_cancel:
                cancel();
                break;
        }
    }
    public void setDialogSize(){
        //获取当前窗口对象
        Window window = getWindow();
//        获取窗口对象参数
        WindowManager.LayoutParams wlp = window.getAttributes();
//        获取屏幕宽度
        Display display = window.getWindowManager().getDefaultDisplay();
        wlp.width = (int)(display.getWidth());//对话框窗口为屏幕窗口
        wlp.gravity = Gravity.TOP;
        window.setBackgroundDrawableResource(android.R.color.transparent);
        window.setAttributes(wlp);
    }
}
