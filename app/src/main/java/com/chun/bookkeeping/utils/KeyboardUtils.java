package com.chun.bookkeeping.utils;

import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.text.Editable;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;

import com.chun.bookkeeping.R;

public class KeyboardUtils {
    private final Keyboard mKeyboard;//自定义键盘
    private KeyboardView mKeyboardView;
    private EditText mEditText;

    public interface OnEnsureListener{
        public void onEnsure();
    }

    private OnEnsureListener onEnsureListener;

    public void setOnEnsureListener(OnEnsureListener onEnsureListener) {
        this.onEnsureListener = onEnsureListener;
    }

    public KeyboardUtils(KeyboardView mKeyboardView, EditText mEditText) {
        this.mKeyboardView = mKeyboardView;
        this.mEditText = mEditText;
        this.mEditText.setInputType(InputType.TYPE_NULL);//取消弹出系统键盘
        mKeyboard = new Keyboard(this.mEditText.getContext(), R.xml.key);

        this.mKeyboardView.setKeyboard(mKeyboard);//设置键盘样式
        this.mKeyboardView.setEnabled(true);
        this.mKeyboardView.setPreviewEnabled(false);
        this.mKeyboardView.setOnKeyboardActionListener(listener);
    }
    //键盘按钮监听
    KeyboardView.OnKeyboardActionListener listener = new KeyboardView.OnKeyboardActionListener() {
        @Override
        public void onPress(int primaryCode) {

        }

        @Override
        public void onRelease(int primaryCode) {

        }

        @Override
        public void onKey(int primaryCode, int[] keyCodes) {
            Editable editable = mEditText.getText();
            int start = mEditText.getSelectionStart();
            switch (primaryCode){
                case Keyboard.KEYCODE_DELETE:
                    if (editable!=null&&editable.length()>0) {
                        if (start>0) {
                            editable.delete(start-1,start);
                        }
                    }
                    break;
                case Keyboard.KEYCODE_CANCEL:
                    editable.clear();
                    break;
                case Keyboard.KEYCODE_DONE:
                    onEnsureListener.onEnsure();//点击确定接口回调
                    break;
                default:
                    editable.insert(start,Character.toString((char)primaryCode));
                    break;
            }
        }

        @Override
        public void onText(CharSequence text) {

        }

        @Override
        public void swipeLeft() {

        }

        @Override
        public void swipeRight() {

        }

        @Override
        public void swipeDown() {

        }

        @Override
        public void swipeUp() {

        }
    };

    //显示键盘
    public void showKeyboard(){
        int visibility = mKeyboardView.getVisibility();
        if(visibility== View.INVISIBLE||visibility==View.GONE){
            mKeyboardView.setVisibility(View.VISIBLE);
        }
    }

    public void hideKeyboard(){
        int visibility = mKeyboardView.getVisibility();
        if(visibility== View.VISIBLE||visibility==View.INVISIBLE){
            mKeyboardView.setVisibility(View.GONE);
        }
    }
}
