package com.chun.bookkeeping.utils;

import java.math.BigDecimal;

public class FloatUtils {
    /**进行除法运算，保留4位小数*/
    public static float div(float v1,float v2){
        float v3 = v1/v2;
        BigDecimal bigDecimal = new BigDecimal(v3);
        float val = bigDecimal.setScale(4, 4).floatValue();
        return val;
    }

    /**讲浮点数类型转换为百分比显示*/
    public static String ratioToPercent(float val){
        float v = val*100;
        BigDecimal bigDecimal = new BigDecimal(v);
        float v1 = bigDecimal.setScale(2, 4).floatValue();
        String per = v1+"%";
        return per;
    }
}
