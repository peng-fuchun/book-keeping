package com.chun.bookkeeping.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.chun.bookkeeping.AboutActivity;
import com.chun.bookkeeping.HistoryActivity;
import com.chun.bookkeeping.MonthChartActivity;
import com.chun.bookkeeping.R;
import com.chun.bookkeeping.SettingActivity;

import java.time.Month;

public class MoreDialog extends Dialog implements View.OnClickListener {
    private Button aboutBtn,settingBtn,historyBtn,infoBtn;
    private ImageView cancelIv;
    public MoreDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_more);
        aboutBtn = findViewById(R.id.btn_dialog_more_about);
        settingBtn = findViewById(R.id.btn_dialog_more_setting);
        historyBtn = findViewById(R.id.btn_dialog_more_record);
        infoBtn = findViewById(R.id.btn_dialog_more_detail);
        cancelIv = findViewById(R.id.iv_dialog_more_cancel);
        aboutBtn.setOnClickListener(this);
        settingBtn.setOnClickListener(this);
        historyBtn.setOnClickListener(this);
        infoBtn.setOnClickListener(this);
        cancelIv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        switch (v.getId()) {
            case R.id.btn_dialog_more_about:
                intent.setClass(getContext(), AboutActivity.class);
                getContext().startActivity(intent);
                break;
            case R.id.btn_dialog_more_setting:
                intent.setClass(getContext(), SettingActivity.class);
                getContext().startActivity(intent);
                break;
            case R.id.btn_dialog_more_record:
                intent.setClass(getContext(), HistoryActivity.class);
                getContext().startActivity(intent);
                break;
            case R.id.btn_dialog_more_detail:
                intent.setClass(getContext(), MonthChartActivity.class);
                getContext().startActivity(intent);
                break;
            case R.id.iv_dialog_more_cancel:
                cancel();
                break;
        }
        cancel();
    }

    /**
     * 设置Dialog尺寸和屏幕尺寸一致
     */
    public void setDialogSize(){
        //获取当前窗口对象
        Window window = getWindow();
//        获取窗口对象参数
        WindowManager.LayoutParams wlp = window.getAttributes();
//        获取屏幕宽度
        Display display = window.getWindowManager().getDefaultDisplay();
        wlp.width = (int)(display.getWidth());//对话框窗口为屏幕窗口
        wlp.gravity = Gravity.BOTTOM;
        window.setBackgroundDrawableResource(android.R.color.transparent);
        window.setAttributes(wlp);
    }
}
