package com.chun.bookkeeping

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.chun.bookkeeping.adapter.AccountAdapter
import com.chun.bookkeeping.db.AccountBean
import com.chun.bookkeeping.db.DBManager
import com.chun.bookkeeping.utils.BudgetDialog
import com.chun.bookkeeping.utils.MoreDialog
import org.w3c.dom.Text
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() ,View.OnClickListener{
    lateinit var todayLv:ListView;//展示今天收支情况的ListView
    lateinit var mDatas:MutableList<AccountBean>;
    lateinit var adapter:AccountAdapter;
    lateinit var searchIv:ImageView;
    lateinit var editBtn:ImageButton;
    lateinit var moreBtn:ImageButton;
    lateinit var preferences: SharedPreferences
    //显示
    var isShow:Boolean = true
    var year:Int = 0
    var month:Int = 0
    var day:Int = 0
    //头布局相关控件
    lateinit var headerView:View
    lateinit var topOutTv:TextView
    lateinit var topInTv:TextView
    lateinit var topBudgetTv:TextView
    lateinit var topConTv:TextView
    lateinit var topShowIv:ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initTime()
        initView();
        preferences = getSharedPreferences("budget", Context.MODE_PRIVATE)
        addLVHearView();
        mDatas = ArrayList();
        //设置适配器：加载每一行数据到列表当中
        adapter = AccountAdapter(this,mDatas)
        todayLv.adapter = adapter
    }

    /**
     * 初始化自带的View的方法
     */
    private fun initView() {
        todayLv = findViewById(R.id.lv_main)
        searchIv = findViewById(R.id.iv_main_search)
        editBtn = findViewById(R.id.ibt_main_edit)
        moreBtn = findViewById(R.id.ibt_main_more)

        searchIv.setOnClickListener(this)
        editBtn.setOnClickListener(this)
        moreBtn.setOnClickListener(this)
        setLVLongClickListener();

    }

    /**
     * 设置ListView长按事件
     */
    private fun setLVLongClickListener() {
        todayLv.setOnItemLongClickListener(object : AdapterView.OnItemLongClickListener{
            override fun onItemLongClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ): Boolean {
                if(position==0){
                    false;
                }
                var pos: Int = position-1
                val clickBean:AccountBean = mDatas[pos]
                //弹出是否删除对话框
                showDeleteItemDialog(clickBean)
                return false
            }
        })
    }

    /**
     * 弹出是否删除某一条记录的对话框
     */
    private fun showDeleteItemDialog(clickBean: AccountBean) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.prompt_message).setMessage(R.string.sure_delete_the_record)
            .setNegativeButton(R.string.cancel,null)
            .setPositiveButton(R.string.ensure
            ) { dialog, which ->
                //执行删除操作
                DBManager.deleteItemFromAccounttbById(clickBean.id)
                mDatas.remove(clickBean)
                adapter.notifyDataSetChanged()
                setTopTvShow()
            }
        builder.create().show()
    }

    /**
     * 添加ListView的头布局
     */
    private fun addLVHearView() {
        //将布局装换成View对象
        headerView = layoutInflater.inflate(R.layout.item_main_top,null)
        todayLv.addHeaderView(headerView)
        topOutTv = headerView.findViewById(R.id.tv_item_top_out_money)
        topInTv = headerView.findViewById(R.id.tv_item_top_in_money)
        topBudgetTv = headerView.findViewById(R.id.tv_item_top_budget_money)
        topConTv = headerView.findViewById(R.id.tv_main_top_today)
        topShowIv = headerView.findViewById(R.id.iv_item_top_hide)

        topBudgetTv.setOnClickListener(this)
        headerView.setOnClickListener(this)
        topShowIv.setOnClickListener(this)

    }

    /**
     * 获取今日具体事件
     */
    private fun initTime() {
        val calendar = Calendar.getInstance()
        year = calendar.get(Calendar.YEAR)
        month = calendar.get(Calendar.MONTH)+1
        day = calendar.get(Calendar.DAY_OF_MONTH)

    }

    /**
     * 当Activity获取焦点时，会调用的方法
     */
    override fun onResume() {
        super.onResume()
        loadDBData();
        setTopTvShow()
    }

    /**
     * 设置头布局文本内容的显示
     */
    private fun setTopTvShow() {
        //获取今日支出和收入总金额 显示在view当中
        val incomeOneDay = DBManager.getSumMoneyOneDay(year, month, day, 1)
        val outcomeOneDay = DBManager.getSumMoneyOneDay(year,month,day,0)
        val infoOneDay = "今日支出 ￥$outcomeOneDay  今日收入 ￥$incomeOneDay"
        topConTv.text = infoOneDay
        //获取本月收入和支出总金额
        val incomeOneMonth = DBManager.getSumMoneyOneMonth(year,month,1)
        val outcomeOneMonth = DBManager.getSumMoneyOneMonth(year,month,0)
        topInTv.text = "￥$incomeOneMonth"
        topOutTv.text = "￥$outcomeOneMonth"
        //设置显示预算剩余
        val budgetMoney:Float = preferences.getFloat("budget", 0f)
        if(budgetMoney==0f){
            topBudgetTv.text = "￥ 0"
        }else{
            val syMoney = budgetMoney-outcomeOneMonth
            topBudgetTv.text = "￥$syMoney"
        }
    }

    private fun loadDBData() {
        val list = DBManager.getAccountListOneDayFromAccounttb(year, month, day)
        mDatas.clear()
        mDatas.addAll(list)
        adapter.notifyDataSetChanged()

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.iv_main_search->{
                val intent = Intent(this,SearchActivity::class.java)
                startActivity(intent)
            }
            R.id.ibt_main_edit->{
                val intent = Intent(this, RecordActivity::class.java)
                startActivity(intent)
            }
            R.id.ibt_main_more->{
                val moreDialog = MoreDialog(this)
                moreDialog.show()
                moreDialog.setDialogSize()
            }
            R.id.tv_item_top_budget_money->{
                showBudgetDialog()
            }
            R.id.iv_item_top_hide->{
                //切换TextView明文和密文
                toggleShow()
            }
        }
        if (v==headerView){
            //头布局被点击了
            val intent = Intent();
            intent.setClass(this,MonthChartActivity::class.java)
            startActivity(intent)
        }
    }

    /**
     * 显示预算设置对话框
     */
    private fun showBudgetDialog() {
        val budgetDialog = BudgetDialog(this)
        budgetDialog.show()
        budgetDialog.setDialogSize()
        budgetDialog.setOnBudgetEnsureListener {
            //将预算金额写入共享参数当中，进行存储
            val editor = preferences.edit()
            editor.putFloat("budget",it)
            editor.commit()

            //计算剩余余额
            var outcomeOneMonth:Float = DBManager.getSumMoneyOneMonth(year,month,0)
            var syMoney = it - outcomeOneMonth
            topBudgetTv.text = "￥$syMoney"
        }
    }

    /**
     * 切换TextView明文和密文
     */
    private fun toggleShow() {
        if (isShow) { //明文->密文
            val passwordTransformationMethod = PasswordTransformationMethod.getInstance()
            topInTv.transformationMethod = passwordTransformationMethod
            topOutTv.transformationMethod = passwordTransformationMethod
            topBudgetTv.transformationMethod = passwordTransformationMethod
            topShowIv.setImageResource(R.mipmap.ih_hide)
            isShow = false
        }else{
            val hideReturnsTransformationMethod = HideReturnsTransformationMethod.getInstance()
            topInTv.transformationMethod = hideReturnsTransformationMethod
            topOutTv.transformationMethod = hideReturnsTransformationMethod
            topBudgetTv.transformationMethod = hideReturnsTransformationMethod
            topShowIv.setImageResource(R.mipmap.ih_show)
            isShow = true
        }
    }
}